//! Serializable structures for WebXDC updates.

use crate::appinfo::AppInfo;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use ts_rs::TS;

/// WebXDC status update.
#[derive(Serialize, Deserialize)]
pub struct WebxdcStatusUpdate {
    /// `payload` field of the WebXDC update.
    pub payload: WebxdcStatusUpdatePayload,
}

/// WebXDC status update payload.
#[derive(Debug, Serialize, Deserialize, TS)]
#[ts(export)]
#[ts(export_to = "frontend/src/bindings/")]
#[serde(tag = "type")]
pub enum WebxdcStatusUpdatePayload {
    /// Request sent from the frontend to the bot
    /// when user clicks "Download"
    /// in the dialog notifying about
    /// the newer version of the `store.xdc`.
    UpdateWebxdc {
        /// Old serial of the store index.
        serial: u32,
    },

    /// Response sent by the bot if the bot receives a request
    /// to a previously sent `store.xdc` with a
    /// different `tag_name` from the current one.
    Outdated {
        /// Always true.
        critical: bool,

        /// `tag_name` field from the `manifest.toml` of the actual `store.xdc`.
        tag_name: String,
    },

    /// Response sent to an outdated version of `store.xdc`
    /// when the user requested a new `store.xdc` with an `UpdateWebxdc` request.
    ///
    /// This response is used by the frontend to display
    /// instructions for the user to look for an updated `store.xdc` in the chat.
    UpdateSent,

    /// Request to update the application index
    /// sent by the `store.xdc` frontend to the bot.
    UpdateRequest {
        /// Requested update sequence number.
        serial: u32,
        /// List of apps selected for caching.
        /// The bot will send an [WebxdcStatusUpdatePayload::DownloadOkay] for each of these apps containing the bundled xdc.
        #[serde(default)]
        apps: Vec<(String, String)>,
    },

    /// Request to download the application .xdc
    /// sent by the frontend to the bot.
    Download {
        /// ID of the requested application.
        app_id: String,
    },

    /// Successful response to the download request.
    DownloadOkay {
        /// `app_id` of the downloaded app.
        app_id: String,

        /// Name to be used as filename in `sendToChat`.
        name: String,

        /// Base64 encoded webxdc.
        data: String,
    },

    /// Negative response to the download request.
    DownloadError {
        /// Application ID of the requested app.
        app_id: String,

        /// Error message.
        error: String,
    },

    /// Index update response sent by the bot to the frontend.
    Update {
        /// List of new / updated app infos.
        #[ts(type = "Record<string, (Partial<AppInfo> & { app_id: string } | null)>")]
        app_infos: Value,

        /// The newest serial of the bot.
        serial: u32,

        /// The old serial that the request was made with.
        /// If it is a full [AppInfo] update, this will be 0.
        old_serial: u32,

        /// `app_id`s of apps that will receive a new .xdc.
        /// The frontend can use these to set the state to updating.
        updating: Vec<String>,

        /// Date as a timestamp in seconds.
        time: i64,
    },

    /// First message send to the store xdc together containing all [AppInfo]s.
    Init {
        /// List of initial [AppInfo]s.
        app_infos: Vec<AppInfo>,

        /// Last serial of the store.
        serial: u32,
    },
}
