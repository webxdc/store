//! Handling WebXDC manifest.

use anyhow::{Context as _, Result};
use async_zip::tokio::read::fs::ZipFileReader;
use serde::Deserialize;

/// `manifest.toml` structure.
#[derive(Deserialize)]
pub struct WebxdcManifest {
    /// Webxdc application identifier.
    pub app_id: String,

    /// Tag Name of the application.
    pub tag_name: String,

    /// Webxdc name, used on icons or page titles.
    pub name: String,

    /// Description of the application.
    pub description: String,

    /// URL of webxdc source code.
    pub source_code_url: String,

    /// Date displayed in the store.
    pub date: String,
}

/// Reads the given ZIP file entry into a string.
async fn read_string(reader: &ZipFileReader, index: usize) -> Result<String> {
    let mut entry = reader.reader_with_entry(index).await?;
    let mut data = String::new();
    entry.read_to_string_checked(&mut data).await?;
    Ok(data)
}

/// Extracts and parses `manifest.toml` from the .xdc ZIP archive.
pub async fn get_webxdc_manifest(reader: &ZipFileReader) -> Result<WebxdcManifest> {
    let entries = reader.file().entries();
    let manifest_index = entries
        .iter()
        .enumerate()
        .find(|(_, entry)| {
            entry
                .entry()
                .filename()
                .as_str()
                .map(|name| name == "manifest.toml")
                .unwrap_or_default()
        })
        .map(|a| a.0)
        .context("Can't find manifest.toml")?;

    Ok(toml::from_str(&read_string(reader, manifest_index).await?)?)
}
