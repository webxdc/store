//! Command line interface.

use clap::{Parser, Subcommand};

/// Command line argument parser.
#[derive(Parser, Debug)]
#[command()]
pub struct BotCli {
    #[allow(clippy::missing_docs_in_private_items)]
    #[command(subcommand)]
    pub action: BotActions,
}

/// Command line subcommands.
#[derive(Subcommand, Debug)]
pub enum BotActions {
    /// Start the bot.
    Start,
    /// Import xdcs.
    Import {
        /// Path from which files should be imported.
        path: String,

        /// Whether the bot should watch the path for changes.
        /// If you want to use this flag you have to give a path to a directory containing a `xdget.lock` file.
        #[arg(long)]
        watch: bool,
    },
    /// Print the bot address and exit.
    Addr,
    /// Show the 1:1-invite QR code.
    ShowQr,
    /// Show the bots version.
    Version,
}
