
## The core `bot.rs` and the chats it manages 

The Bot maintains 1:1 chats with each of its users
who can get in contact via a QR code or by sending a text message to the bot.

Bot user can use 1:1 chat to discover, download and use webxdcs from the store.
Upon receiving a 1:1 chat message _or_ when a `DC::Contact` is verified with a QR-code, 
the bot sends the `store.xdc` frontend to the user.

Currently implemented Bot/Frontend interactions are: 

    - Updating the store.xdc
    - Receive updates to the app index
    - Searching the app index 
    - Downloading apps
    - Sharing an app to a chat

Requests, be it `webxdcStatusUpdate`s or normal messages, are handled in `src/store.rs`.
The dedicated handlers receive a `State` instance which holds the bot-state (for example a connection pool to the DB)
and the Deltachat `Context` object which is necessary to send messages, receive chats, etc. etc.

## App Ids

The bot uses two kinds of ids: One is the database's `row-id` and the other one is the `app-id`
taken from the webxdcs `manifest.toml` or the `xdget.lock` file. Internally, the `row-id` is used
as an unambiguous identifier for each `app-info` which is stored in the database.
The newly added `app-id` should only be used to uniquely identify newly added webxdcs and their
different versions. When adding a webxdc, the `app-id` is used to distinguish between
app-upgrading requests and requests which add a new webxdc to the store.

## Updating the App Index

Updating the App Index happens by importing newer versions of webxdc app files with the CLI. 
For every added webxdc app the `app-id` and `tag_name` are checked and a new `AppInfo` is created 
and added to the bot database. 
If there already is an `AppInfo` with the same `app-id` but an older version, 
then only the newer `AppInfo` version will be served to the frontend. 

Every change to the App Index increases a "serial" 
which the frontend and bot use for synchronization, see `Synchronizing the App Index`. 

## Frontend

The frontend is built with `SolidJs` as this framework is compiled and produces very fast
and more importantly small webxdc apps. Most of the styling is done with `uno-css`
which is a tailwind-like CSS utility framework.
Only some exceptions are contained in the `index.sass` file.

The frontent `store.xdc` is build by the bundler and works on some stripped version 
of the Rust `AppInfo` struct. Private fields like `xdc-blob-dir` are removed, so that only the
needed fields are sent to the frontend.
On the frontend, the `row-id` (AppInfo::id) is used to distinguish different AppInstances.
For example for caching.

## Synchronizing the App Index

The app index describes the list of apps that are shown in the frontend webxdc.
When the `store.xdc` frontend is initially sent to a user's device, 
the bot also sends a `webxdcStatusUpdate` containing the current list of active `AppInfos` 
and the latest serial.
When the `store.xdc` frontend requests new updates it sends its current app-index serial 
and the bot will send updates for all `AppInfos` with a serial greater than that last seen serial.
These updates are sent as a json-tree where only changed fields are listed.
The `store.xdc` update all apps according to this update message and remove apps that have a value of `null`.
An example update could look like this: 
```json
{
    "tower_builder": {
        "tag_name": "v12.1",
        "source_code_url": "https://github.com/webxdc/tower-builder"
    },
    "2048": null
}
```
